============
大模型推理
============

.. toctree::
   :maxdepth: 1

   Docker部署-快速开始教程 <../../server/docs/general_model_inference.md>
   大模型推理教程 <inference_index.rst>
   实践调优 <infer_optimize.rst>
   静态图模型列表 <../../server/docs/static_models.md>
   各个模型推理量化教程 <models.rst>
   异构设备推理 <devices.rst>
