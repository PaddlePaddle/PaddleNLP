============
大模型推理教程
============

.. toctree::
   :maxdepth: 1

   算子安装 <installation.md>
   大模型推理教程 <inference.md>
   静态图高性能部署全流程 <../../server/docs/deploy_usage_tutorial.md>
